var number = 1;

function increa() {
    number += 1;
    changeQuantity(number);
}

function decrea() {
    number -= 1;
    if (number < 1) {
        number = 1;
    }
    changeQuantity(number);
}

function changeQuantity(param) {
    document.getElementById('quantity-value').innerText = param;
}

changeQuantity(number);

var info = {
    cost: 0.06,
    max: 10,
    presale: 'December 14, 7: 00 PM',
    mainsale: 'December 16, 7:00 PM'
}

function changeInfo(param) {
    document.getElementById('cost').innerText = param.ost;
    document.getElementById('max').innerText = param.max;
    document.getElementById('presale').innerText = param.presale;
    document.getElementById('mainsale').innerText = param.mainsale;
}

changeInfo(info);